#!/bin/bash
# Purpose: Wget Gutenberg books from Read Comma Separated CSV File
# Author: Vivek Gite under GPL v2.0+ (CSV script)
# Author: Jose Maria Madronal GPL v2.0+ (Proxmox script)
# ------------------------------------------
#
INPUT=Top100ProjectGutenbergV01.csv
OLDIFS=$IFS
IFS=','
[ ! -f $INPUT ] && { echo "$INPUT file not found"; exit 99; }
while read id titol
do
	echo "ID :-$id-"
	echo "TITOL :-$titol-"
	# wget 
	wget https://www.gutenberg.org/files/$id/$id-0.txt -O $titol.txt
done < $INPUT
IFS=$OLDIFS
